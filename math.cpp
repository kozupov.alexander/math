#include <iostream>

void function(int N, bool even)
{
	
	for (int i = even; i < N; i += 2)
	{

		std::cout << " " << i;

	}
	
}

int main()
{
    int p = 0;

	while (p < 17)
	{
		p++;

		if (p % 2 == 0)

			std::cout << " " << p;
	}

	std::cout << "\n";

	function(19, true);
}
	
